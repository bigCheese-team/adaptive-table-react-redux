# Functionality: #

## Sorting by columns: ##
when you click on a column name, the table rows are sorted in ascending order, when you click again, in descending order. 

## Client-side pagination: ##
the data must be displayed on a page-by-page basis, with a maximum of 25 elements per page. You must provide custom navigation to navigate through the pages.

## Filtering: ##
the component provides a text field in which the user can enter text and the rows of the table, the data of which does not contain the substring entered by the user, are hidden. 
By clicking on a row in the table, the field values ​​are displayed in an additional block below the table.
The data is loaded into the table from the server.


### How do I get set up? ###
 npm install
 npm start 
 
For inspect React component install redux devtools extension to your browser
https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi

See project:
 http://petersdorf.ru/redux_table/
