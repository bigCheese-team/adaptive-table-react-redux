

export default class GotService {
    constructor() {
        this._url = 'http://www.filltext.com/?rows=100&id={number|1000}&firstName={firstName}&lastName={lastName}&email={email}&phone={phone|(xxx)xxx-xx-xx}&adress={addressObject}&description={lorem|32}'
    }

    async getResource() {
        const res  = await fetch(this._url,
            {
                headers: new Headers({
                    'Content-Type' : 'application/json',
                    'Accept-Charset' : 'utf-8',
                })
            })
        if(!res.ok) {
            throw new Error(`Could not fetch url: ${this._url} \n 
            Error: ${res.status}`)
        }
        return await res.json()
    }
}

