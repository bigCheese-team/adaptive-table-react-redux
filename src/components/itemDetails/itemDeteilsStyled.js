import styled from 'styled-components';

export  const DetailsCard = styled.div`
     width: 100%;
    margin: auto;
    background: #b3ac96;
    padding: 5px;
    color: #3e444a;
    min-height: 200px
`;