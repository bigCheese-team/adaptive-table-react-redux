import React from "react";
import {DetailsCard} from "./itemDeteilsStyled";
import {Container, Row, Col} from "reactstrap";


const ItemDetails = (props) => {
    const {firstName, lastName, email, streetAddress, city, state, zip , phone, description} = props.details
    const rowStyle = props.details.firstName === undefined? 'd-none': 'd-flex m-4'
    return (
        <Container id="description">
            <DetailsCard>
                <Row className={rowStyle}>
                    <Col md="8" sm="12" className="text-left">
                        <h3>{`${firstName} ${lastName}`}</h3>
                        <p>{`Phone: ${phone}`}</p>
                        <p>{`Email: ${email}`}</p>
                        <h4>Adress:</h4>
                        <p>{
                            `State: ${state}; 
                        City: ${city}; 
                        StreetAddress: ${streetAddress};
                        Zip: ${zip}`}</p>
                    </Col>
                    <Col md="4" sm="12" className="text-justify">
                        {description}
                    </Col>
                </Row>
            </DetailsCard>
        </Container>
    )
}

export default ItemDetails