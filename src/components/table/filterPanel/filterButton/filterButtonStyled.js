import styled from 'styled-components';

export const TableHead = styled.th`
&:hover {
    background: #d29c9c;
    color: #3e444a;
}
`