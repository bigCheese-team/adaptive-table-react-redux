import React, {useState} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretDown, faCaretUp } from '@fortawesome/free-solid-svg-icons'
import {TableHead} from './filterButtonStyled'


const FilterButton = ({filter, getSortFromLarger, getSortFromSmallest, onFilter}) => {
    const [count, setCount] = useState(true);

    const filtrate = (filterName) => {
        if (!count) {
            getSortFromLarger(filterName)
            setCount(!count)
        } else {
            getSortFromSmallest(filterName)
            setCount(!count)
        }
    }
    return (
        <TableHead onClick={() => {
            filtrate(filter.name)
            onFilter(filter.name)
        }}>
            {filter.isChoose? count? <FontAwesomeIcon icon={faCaretDown} /> : <FontAwesomeIcon icon={faCaretUp} />: null }
            <div  >{filter.name}</div>
        </TableHead>
    )
}

export default FilterButton