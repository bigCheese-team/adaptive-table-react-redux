import React, {Component} from 'react';
import FilterButton from "./filterButton/";


export default class FilterPanel extends Component {
    constructor(props) {
        super(props)
        this.state = {
            filters: [
                {name: "Id", isChoose: true},
                {name: "Name", isChoose: false},
                {name: "Adress", isChoose: false},
                {name: "Phone", isChoose: false},
                {name: "Email", isChoose: false},
                {name: "Description", isChoose: false}
            ]
        }
    }


 onFilter = (filterName) => {
     const {filters} = this.state
     const indSelectedItem = filters.findIndex(elem => elem.isChoose === true)
     const unSelectedElem = filters[indSelectedItem]
     unSelectedElem.isChoose = false
     const originalFilters = [...filters.slice(0, indSelectedItem), unSelectedElem, ...filters.slice(indSelectedItem + 1, filters.length)]
     console.log(filterName)
     const indSelectItem = originalFilters.findIndex(elem => elem.name === filterName)
     const selectedElem = originalFilters[indSelectItem]
     selectedElem.isChoose = true
     const newFilters = [...originalFilters.slice(0, indSelectItem), selectedElem, ...originalFilters.slice(indSelectItem + 1, originalFilters.length)]
     this.setState({
         filters:newFilters
     })
    }


    render() {

        const { getSortFromLarger,  getSortFromSmallest} = this.props
        const {filters} = this.state
        console.log(filters)
        const filtersPanelElement = filters.map((elem, i) => {
            return (
                <FilterButton key={i}
                              onFilter={this.onFilter}
                              filter={elem}
                              getSortFromSmallest={getSortFromSmallest}
                              getSortFromLarger={getSortFromLarger}/>
            )
        })
        return (
            <tr>
                {filtersPanelElement}
            </tr>
        )
}

}

