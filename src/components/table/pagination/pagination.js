import React from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import {Wrapper} from './styledPagination.js'
import './pagination.css'

const PaginationComponent = (props) => {
    const {actualPage,  numbersOfPages, onChangeActualPage} = props
    const linkOnPageOne =  actualPage === numbersOfPages? actualPage - 2: actualPage > 1 ? actualPage - 1: actualPage
    const linkOnPageSecond = actualPage === 1? actualPage + 1: actualPage === numbersOfPages? actualPage - 1: actualPage
    const linkOnPageThird = actualPage === numbersOfPages? actualPage: actualPage > 1 ? actualPage + 1: actualPage + 2
    const linkVisibilityOnePage = numbersOfPages >= 2? "d-block": "d-none"
    const linkVisibilityManyPages = numbersOfPages >= 3? "d-block": "d-none"
    return (
        <Wrapper>
            <Pagination  style={{justifyContent: 'center'}} size="lg" aria-label="Page navigation">
                <PaginationItem >
                    <PaginationLink  onClick={() => onChangeActualPage(1)}
                                    className={actualPage <= 2? "d-none": linkVisibilityManyPages}
                                    href="#">
                        1
                    </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink onClick={() => onChangeActualPage(actualPage - 1)}
                                    className={actualPage === 1? "d-none": linkVisibilityOnePage}
                                    previous
                                    href="#" />
                </PaginationItem>
                <PaginationItem active={actualPage === 1}>
                    <PaginationLink onClick={() => onChangeActualPage(linkOnPageOne)}
                                    href="#">
                        {linkOnPageOne}
                    </PaginationLink>
                </PaginationItem>
                <PaginationItem active={actualPage>1 && actualPage < numbersOfPages}>
                    <PaginationLink onClick={() => onChangeActualPage(linkOnPageSecond)}
                                    className={linkVisibilityOnePage}
                                    href="#">
                        {linkOnPageSecond}
                    </PaginationLink>
                </PaginationItem>
                <PaginationItem active={actualPage === numbersOfPages}>
                    <PaginationLink onClick={() => onChangeActualPage(linkOnPageThird)}
                                    className={linkVisibilityManyPages}
                                    href="#">
                        {linkOnPageThird}
                    </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink onClick={() => onChangeActualPage(actualPage + 1)}
                                    className={ numbersOfPages - actualPage < 2?"d-none": linkVisibilityManyPages}
                                    next
                                    href="#" />
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink onClick={() => onChangeActualPage(numbersOfPages)}
                                    className={numbersOfPages - actualPage < 2?"d-none": linkVisibilityManyPages}
                                    href="#">
                        {numbersOfPages}
                    </PaginationLink>
                </PaginationItem>
            </Pagination>
        </Wrapper>
    );
}

export default PaginationComponent;