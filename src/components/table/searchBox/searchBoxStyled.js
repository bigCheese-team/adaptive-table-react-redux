import styled from "styled-components";

export const TitleH1 = styled.h1`
    color: #b3ac96;
    font-size: 2em;
`

export const SearchButton = styled.button`
    background: #b3ac96;
    border: 1px solid #b3ac96;
    color: #3e444a;
    &:hover {
        background: #d29c9c;
        border-color: #d29c9c;
    }
`

export const SearchInput = styled.input`
    background: #343a40;
    border: 1px solid #b3ac96;
    color: #b3ac96;
`