import React, { useState } from 'react';
import { TitleH1, SearchInput, SearchButton } from './searchBoxStyled'
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
} from 'reactstrap';


const SearchBox = (props) => {
    const {lookFor, onUpdateSearchWord} = props
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <div>
            <Navbar color="dark" light expand="md">
                <NavbarBrand href="/">
                    <TitleH1>
                        Table
                    </TitleH1>
                </NavbarBrand>
                <NavbarToggler onClick={toggle} />
                <Collapse isOpen={isOpen} navbar>
                    <Nav className="mr-auto" navbar>

                    </Nav>
                    <SearchInput onChange={onUpdateSearchWord} type="text" placeholder="Search" />
                    <SearchButton onClick={() => lookFor()} primary>look for</SearchButton>
                </Collapse>
            </Navbar>
        </div>
    );
}

export default SearchBox;