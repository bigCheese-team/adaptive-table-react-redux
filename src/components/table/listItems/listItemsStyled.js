import styled from "styled-components";


export const Call = styled.td`
    font-size: 1em;
    text-align: center;
    color: ${prop => prop.id? '#d29c9c' : '#b3ac96'};
    @media (max-width: 768px) {
        display: flex;
        width: 100%;
        border-top: none !important;
}
`

export const TableRow = styled.tr`
&:hover {
    background: #4e3636;
    }
@media (max-width: 768px) {
    border-bottom: 2px solid #5dc1a6 !important;

 }
`