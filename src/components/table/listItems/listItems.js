import React from 'react';
import { Call, TableRow } from './listItemsStyled'

const ListItems = (props) => {

    const { getDetails } = props
    const {id, firstName, lastName, email, adress, phone, description} = props.item
    const {streetAddress, city, state, zip} = adress
    return(
        <TableRow onClick={() => {
            getDetails({id, firstName, lastName, email, streetAddress, city, state, zip, phone, description})
            window.scrollBy(0, window.innerHeight );
        }} >
            <Call>{id}</Call>
            <Call>{`${firstName} ${lastName}`}</Call>
            <Call>{
                `State: ${state} \n
                City: ${city} \n
                StreetAddress: ${streetAddress}`}</Call>
            <Call>{phone}</Call>
            <Call>{email}</Call>
            <Call>{description}</Call>
        </TableRow>
    )
}

export default ListItems;
