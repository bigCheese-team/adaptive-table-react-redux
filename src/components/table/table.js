import React, {Component} from "react";
import SearchBox from "./searchBox/searchBox";
import Pagination from "./pagination/pagination";
import ListItem from './listItems/listItems'
import { searchByString,
    sortFromLarger,
    sortFromSmallest} from "../../store/actions/actions";
import {connect} from "react-redux";
import { Table } from 'reactstrap';
import Spinner from "../spinner";
import ItemDetails from "../itemDetails";
import styled from 'styled-components/macro';
import FilterPanel from './filterPanel'

const TableHead = styled.thead`
        color: #d29c9c;
        @media (max-width: 768px) {
            display: none;
            
         }
       `;

const TableContainer = styled.div`
    width: 100%;
    min-height: 500px;
`

class TableArea extends Component{
    constructor() {
        super();
        this.state = {
            pageNumber: 1,
            totalNumberOfPages: 0,
            numberItemsOnPage: 10,
            details: {},
            searchWord: ''
        }
    }

    onChangeActualPage = (number) => {
        this.setState({
            pageNumber: number
        })
    }

    getDetails = (item) => {
        console.log(item)
        this.setState({
            details:item
        })
    }

    onUpdateSearchWord = (e) => {
        this.setState({
            searchWord: e.target.value
        })
    }

    lookFor = () => {
        const {searchWord} = this.state
        const {searchByString} = this.props
        const {data} = this.props.store
        searchByString(searchWord, data)
    }

    getSortFromLarger = (colName) => {
        const {data} = this.props.store
        const {sortFromLarger} = this.props
        sortFromLarger(data, colName)
    }
    getSortFromSmallest = (colName) => {
        const {data} = this.props.store
        const {sortFromSmallest} = this.props
        sortFromSmallest(data, colName)
    }

    renderTemplate = () => {
        const {pageNumber, numberItemsOnPage} = this.state
        const {data} = this.props.store
        const tableRow = data.map((item, i) => {
            if(pageNumber * numberItemsOnPage - numberItemsOnPage <= i && pageNumber * numberItemsOnPage > i) {
                return item.isVisible === undefined || item.isVisible === true
                    ? <ListItem key={i} item={item} getDetails={this.getDetails}/>: null
            }
    })
    return tableRow
    }

    render() {
        const {dataLength, loading} = this.props.store
        const {pageNumber,
            numberItemsOnPage,
            details} = this.state

        const content = loading? <Spinner />: (
            <Table responsive>
                <TableHead>
                    <FilterPanel getSortFromLarger={this.getSortFromLarger}
                                 getSortFromSmallest={this.getSortFromSmallest}/>
                </TableHead>
                <tbody>
                {this.renderTemplate()}

                </tbody>
            </Table>

        )

        return (
            <div>
                <TableContainer>
                    <SearchBox lookFor={this.lookFor}
                               onUpdateSearchWord={this.onUpdateSearchWord}/>

                    {content}
                    <Pagination actualPage={pageNumber}
                                numbersOfPages={Math.ceil(dataLength / numberItemsOnPage)}
                                onChangeActualPage={this.onChangeActualPage}/>

                </TableContainer>
                <ItemDetails details={details} />
            </div>
        );
    }
}


const mapStateToProps = (store) => {
    console.log("store", store)
    return {
        store: store
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        searchByString: (str, data) => dispatch(searchByString(str, data)),
        sortFromLarger: (data, colName) => dispatch(sortFromLarger(data, colName)),
        sortFromSmallest: (data, colName) => dispatch(sortFromSmallest(data, colName))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TableArea);