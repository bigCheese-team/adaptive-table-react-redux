import React from 'react';
import { Spinner } from 'reactstrap';
import {LoaderStyled} from './spinnerStyled'

const Loader = () => {
    return (
           <LoaderStyled>
               <Spinner style={{ width: '3rem', height: '3rem' }} type="grow" color="light" />
           </LoaderStyled>
    )
}

export default Loader;