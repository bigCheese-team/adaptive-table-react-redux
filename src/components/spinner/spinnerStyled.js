import styled from 'styled-components';

export const LoaderStyled = styled.div`
    margin-top: 34vh;
    margin-bottom: 40vh;
`