import {
    GET_DATA_SUCCESS,
    GET_DATA_STARTED,
    GET_DATA_FAIL,
    SEARCH_BY_INDEX_OF,
    SORT_ID_LARGER,
    SORT_ID_SMALLEST,
    SORT_NAME_LARGER,
    SORT_NAME_SMALLEST,
    SORT_ADRESS_LARGER,
    SORT_ADRESS_SMALLEST,
    SORT_PHONE_LARGER,
    SORT_PHONE_SMALLEST,
    SORT_EMAIL_LARGER,
    SORT_EMAIL_SMALLEST,
    SORT_DESCRIPTION_LARGER,
    SORT_DESCRIPTION_SMALLEST
} from './actionsType'
import GotService from "../../service/servis";

const gotService = new GotService()

export const getData = () => {

  return dispatch => {
      dispatch(getDataStarted())
      gotService.getResource()
          .then(res => {
              dispatch(getDataSuccess(res))
          })
          .catch(res => {
              dispatch(getDataFail(res.message))
          })
  }
}

const getDataStarted = () => ({
    type: GET_DATA_STARTED
})

const getDataSuccess = (data) => ({
    type: GET_DATA_SUCCESS,
    data: data,
    payload: data.length
})

const getDataFail = (error) => ({
    type: GET_DATA_FAIL,
    payload: error
})

const _findIn = (item, str) => {
    return item.indexOf(str) !== -1 ? true: false
}


export const searchByString = (str, data) => {
    // let newData = []
    let visibleItemCount = 0
    const newData = data.map(item => {
        const {firstName, lastName, email, adress, phone, description} = item
        if(_findIn(firstName, str)
            || _findIn(lastName, str)
            || _findIn(email, str)
            || _findIn(description, str)) {
            item.isVisible = true
            visibleItemCount++
        } else {
            item.isVisible = false
        }
        return item
    })
    return {
        type: SEARCH_BY_INDEX_OF,
        visibleItems: newData,
        payload: visibleItemCount
    }
}

export const sortFromLarger = (data, colName) => {
    console.log("LARGER")
    let newData = []
    switch (colName) {
        case 'Id':
            newData = data.sort((a, b) => {
                if(a.id < b.id) {
                    return 1
                }
                if(a.id > b.id) {
                    return -1
                }
                return 0
            })
            return {
                type: SORT_ID_LARGER,
                sortData: newData
            }
        case 'Name':
            newData = data.sort((a, b) => {
                if(a.firstName < b.firstName) {
                    return 1
                }
                if(a.firstName > b.firstName) {
                    return -1
                }
                return 0
            })
            return {
                type: SORT_NAME_LARGER,
                sortData: newData
            }
        case 'Adress':
            newData = data.sort((a, b) => {
                if(a.adress.city < b.adress.city) {
                    return 1
                }
                if(a.adress.city > b.adress.city) {
                    return -1
                }
                return 0
            })
            return {
                type: SORT_ADRESS_LARGER,
                sortData: newData
            }
        case 'Phone':
            newData = data.sort((a, b) => {
                if(a.phone < b.phone) {
                    return 1
                }
                if(a.phone > b.phone) {
                    return -1
                }
                return 0
            })
            return {
                type: SORT_PHONE_LARGER,
                sortData: newData
            }
        case 'Email':
            newData = data.sort((a, b) => {
                if(a.email < b.email) {
                    return 1
                }
                if(a.email > b.email) {
                    return -1
                }
                return 0
            })
            console.log(newData)
            return {
                type: SORT_EMAIL_LARGER,
                sortData: newData
            }
        case 'Description':
            newData = data.sort((a, b) => {
                if(a.description < b.description) {
                    return 1
                }
                if(a.description > b.description) {
                    return -1
                }
                return 0
            })
            console.log(newData)
            return {
                type: SORT_DESCRIPTION_LARGER,
                sortData: newData
            }
        default:
            return {
                sortData: data
            }
    }
}

export const sortFromSmallest = (data, colName) => {
    console.log("SMALLEST")
    let newData = []
    switch (colName) {
        case 'Id':
            newData = data.sort((a, b) => {
                if(a.id < b.id) {
                    return -1
                }
                if(a.id > b.id) {
                    return 1
                }
                return 0
            })
            console.log(newData)
            return {
                type: SORT_ID_SMALLEST,
                sortData: newData
            }
        case 'Name':
            newData = data.sort((a, b) => {
                if(a.firstName < b.firstName) {
                    return -1
                }
                if(a.firstName > b.firstName) {
                    return 1
                }
                return 0
            })
            console.log(newData)
            return {
                type: SORT_NAME_SMALLEST,
                sortData: newData
            }
        case 'Adress':
            newData = data.sort((a, b) => {
                if(a.adress.city < b.adress.city) {
                    return -1
                }
                if(a.adress.city > b.adress.city) {
                    return 1
                }
                return 0
            })
            console.log(newData)
            return {
                type: SORT_ADRESS_SMALLEST,
                sortData: newData
            }
        case 'Phone':
            newData = data.sort((a, b) => {
                if(a.phone < b.phone) {
                    return -1
                }
                if(a.phone > b.phone) {
                    return 1
                }
                return 0
            })
            console.log(newData)
            return {
                type: SORT_PHONE_SMALLEST,
                sortData: newData
            }
        case 'Email':
            newData = data.sort((a, b) => {
                if(a.email < b.email) {
                    return -1
                }
                if(a.email > b.email) {
                    return 1
                }
                return 0
            })
            console.log(newData)
            return {
                type: SORT_EMAIL_SMALLEST,
                sortData: newData
            }
        case 'Description':
            newData = data.sort((a, b) => {
                if(a.description < b.description) {
                    return -1
                }
                if(a.description > b.description) {
                    return 1
                }
                return 0
            })
            console.log(newData)
            return {
                type: SORT_DESCRIPTION_SMALLEST,
                sortData: newData
            }
        default:
            return {
                sortData: data
            }
    }
}
