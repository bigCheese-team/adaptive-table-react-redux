import {
    GET_DATA_SUCCESS,
    GET_DATA_STARTED,
    GET_DATA_FAIL,
    SEARCH_BY_INDEX_OF,
    SORT_ID_LARGER,
    SORT_ID_SMALLEST,
    SORT_NAME_LARGER,
    SORT_NAME_SMALLEST,
    SORT_ADRESS_LARGER,
    SORT_ADRESS_SMALLEST,
    SORT_PHONE_LARGER,
    SORT_PHONE_SMALLEST,
    SORT_EMAIL_LARGER,
    SORT_EMAIL_SMALLEST,
    SORT_DESCRIPTION_LARGER,
    SORT_DESCRIPTION_SMALLEST
} from './actions/actionsType'

let initialState = {
    data: [],
    dataLength: 0,
    loading: false,
    error: null
}


const reducer = (state = initialState, action) => {

    switch(action.type) {
        case GET_DATA_SUCCESS:
            return {
                data: action.data,
                loading: false,
                error: null,
                dataLength: action.payload
            }
        case GET_DATA_STARTED:
            return {
                ...state,
                loading: true
            }
        case GET_DATA_FAIL:
            return {
                ...state,
                error: action.payload
            }
        case SEARCH_BY_INDEX_OF:
            return {
                ...state,
                data: action.visibleItems,
                dataLength: action.payload
            }
        case SORT_ID_LARGER:
            return {
                ...state,
                data: action.sortData
            }
        case SORT_ID_SMALLEST:
            return {
                ...state,
                data: action.sortData
            }
        case SORT_NAME_LARGER:
            return {
                ...state,
                data: action.sortData
            }
        case SORT_NAME_SMALLEST:
            return {
                ...state,
                data: action.sortData
            }
        case SORT_ADRESS_LARGER:
            return {
                ...state,
                data: action.sortData
            }
        case SORT_ADRESS_SMALLEST:
            return {
                ...state,
                data: action.sortData
            }
        case SORT_PHONE_LARGER:
            return {
                ...state,
                data: action.sortData
            }
        case SORT_PHONE_SMALLEST:
            return {
                ...state,
                data: action.sortData
            }
        case SORT_EMAIL_LARGER:
            return {
                ...state,
                data: action.sortData
            }
        case SORT_EMAIL_SMALLEST:
            return {
                ...state,
                data: action.sortData
            }
        case SORT_DESCRIPTION_LARGER:
            return {
                ...state,
                data: action.sortData
            }
        case SORT_DESCRIPTION_SMALLEST:
            return {
                ...state,
                data: action.sortData
            }


        default:
            return state
    }
}
export default reducer