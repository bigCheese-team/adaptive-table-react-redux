import React from 'react';
import './App.css';
import TableArea  from "./components/table/table";
import Store from './store/store'
import {Provider} from 'react-redux'

function App() {

  return (
    <div className="App">
        <Provider store={Store}>
            <TableArea />
        </Provider>
    </div>
  );
}

export default App;
